CREATE TABLE goods (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT NOT NULL UNIQUE,
  price INTEGER NOT NULL CHECK (price >= 0),
  quantity INTEGER NOT NULL CHECK (quantity >= 0) DEFAULT 0
);

CREATE TABLE managers (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT NOT NULL,
  login TEXT NOT NULL UNIQUE,
  salary INTEGER NOT NULL CHECK (salary >= 0)
);

CREATE TABLE sales (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  good_id INTEGER NOT NULL REFERENCES goods,
  manager_id INTEGER NOT NULL REFERENCES managers,
  price INTEGER NOT NULL CHECK (price >= 0),
  quantity INTEGER NOT NULL CHECK (quantity > 0)
);

INSERT INTO goods VALUES (1, 'BigMac', 160, 20);
INSERT INTO goods (name, price, quantity) VALUES ('BigTasty', 180, 20);
INSERT INTO goods (name, price, quantity) VALUES
  ('Free', 60, 60),
  ('Cola', 60, 20),
  ('ChickenBurger', 80, 50),
  ('Tea', 60, 20);

UPDATE goods
SET price = 120, quantity = 15
WHERE id = 1;

UPDATE goods
SET price = 120, quantity = 20
WHERE name = 'BigMac';

SELECT id, name, quantity, price FROM goods;

SELECT * FROM goods;

SELECT id, name, price FROM goods WHERE quantity > 20;

UPDATE goods SET quantity = 0 WHERE id = 5;

SELECT id, name, price FROM goods WHERE quantity > 0 LIMIT 3;

SELECT id, name, price FROM goods WHERE quantity > 0 LIMIT 3 OFFSET 1;

SELECT id, name, price FROM goods ORDER BY price;

SELECT id, name, price FROM goods ORDER BY price DESC;

SELECT id, name, price, quantity FROM goods ORDER BY price DESC, quantity DESC;

SELECT id, name, price * quantity FROM goods;

SELECT id, name, price * quantity AS total FROM goods ORDER BY total DESC;

SELECT id, name, price * quantity total FROM goods ORDER BY total DESC;

SELECT id, upper(name) name FROM goods;

SELECT sum(quantity) FROM goods;

SELECT sum(quantity * price) FROM goods;

SELECT avg(price) FROM goods;

SELECT count(*) FROM goods;

SELECT count(*) FROM goods WHERE price > 100;

DROP TABLE managers;

CREATE TABLE managers (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT NOT NULL,
  login TEXT NOT NULL UNIQUE,
  salary INTEGER CHECK (salary >= 0) NOT NULL,
  boss_id INTEGER REFERENCES managers CHECK (id != boss_id)
);

INSERT INTO managers (name, login, salary, boss_id) VALUES
  ('Vasya', 'vasya', 50000, NULL),
  ('Petya', 'petya', 30000, 1),
  ('Sasha', 'sasha', 25000, 2),
  ('Ruslan', 'ruslan', 25000, 2),
  ('Alex', 'alex', 15000, 4);

SELECT id, name FROM managers WHERE boss_id IS NOT NULL;

SELECT count(*) FROM managers WHERE boss_id IS NOT NULL;

SELECT avg(salary) FROM managers;

SELECT sum(salary) FROM managers;

SELECT count(*), salary FROM managers GROUP BY salary;
SELECT count(*), salary FROM managers WHERE boss_id IS NOT NULL GROUP BY salary;

SELECT max(price) FROM goods;
SELECT min(price) FROM goods;

INSERT INTO sales (good_id, manager_id, price, quantity) VALUES
  (1, 1, 120 , 2),
  (1, 2, 160 , 4),
  (2, 2, 50 , 10),
  (3, 3, 70 , 3),
  (5, 5, 40 , 20);

-- сколько всего продаж сделано (общее количество продаж - важно: не единиц товара)
SELECT count(*) FROM sales;

-- на сколько продано (общая сумма продаж)
SELECT sum(price * quantity) FROM sales;

-- сколько всего единиц товара продано
SELECT sum(quantity) FROM sales;

-- id товара + сумма продаж по нему
SELECT good_id, sum(price * quantity) total FROM sales GROUP BY good_id;

-- id менеджера + сумма его продаж
SELECT manager_id, sum(price * quantity) total FROM sales GROUP BY manager_id;

-- сумма самой дорогой продажи
SELECT max(price * quantity) maximum FROM sales;

-- сумма самой дешёвой продажи
SELECT min(price * quantity) minimum FROM sales;

-- средняя сумма одной продажи
SELECT avg(price * quantity) average FROM sales;

-- средняя сумма продажи по каждому менеджеру (должно быть 4 строки)
SELECT manager_id, avg(price * quantity) average FROM sales GROUP BY manager_id;

-- среднее количество продаж по каждому менеджеру (должно быть 4 строки)
SELECT count(*) AS count, manager_id FROM sales GROUP BY manager_id;
SELECT avg(quantity) average, manager_id FROM sales GROUP BY manager_id;

-- максимальная сумма продаж по каждому менеджеру (должно быть 4 строки)
SELECT manager_id, max(price * quantity) maximum FROM sales GROUP BY manager_id;